@echo off
SET ADDPATH=

FOR %%J IN (lib\*.jar) DO SET ADDPATH=!ADDPATH!%%J;

echo ClassPath: "%ADDPATH%"

java -cp "%ADDPATH%" com.invel.jsincro.Main

rem SET ADDPATH=

pause